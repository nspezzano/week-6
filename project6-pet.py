import pet

myPetName = input("Please type in the name of your pet. ")
myPetType = input("Please type in the age of your pet. ")
myPetAge= input("Please type in the kind of animal your pet is. ")

myPet = pet.Pet(myPetName, myPetAge, myPetType)


print("The name of your pet is", myPet.get_name())
print("The age of your pet is", str(myPet.get_age()), "years old." )
print("Your animal is a", myPet.get_animal_type())
