class Car:

    def __init__(self, year_model, make, speed, brake, accelerate):
        self.__year_model = year_model
        self.__make = make
        self.__speed = speed
        self.__brake = brake
        self.__accelerate = accelerate

    def get_year_model(self):
        return self.__year_model

    def get_make(self):
        return self.__make

    def get_speed(self):
        return self.__speed

    def get_brake(self):
        return self.__brake

    def get_accelerate(self):
        return self.__accelerate

    def set_year_model(self, year_model):
        self.__year_model = year_model

    def set_make(self, make):
        self.__make = make

    def set_speed(self, speed):
        self.__speed = speed
        speed = 0

    def set_brake(self, brake):
        self.__brake = brake
        brake = speed - 5

    def set_accelerate(self, accelerate):
        self.__accelerate = accelerate
        accelerate = speed + 5 

    
