import question

print("Welcome to my trivia game. Each player must switch off answering questions. Please decide who is player one and who is player two. ")

player1 = input("Please type in the name of player 1. ")
player2 = input("Please type in the name of player 2. ")

questions = ("In what country was Christmas once illegal?", "How many dimples are there on a regular golf ball?", "How many punds of pressure do you need to rip off your ear?", "What are the odds of being killed by space debris?", "How much does the earth weigh?", "The 100 years war was fought between what two countries?", "Who was the main author of the Declaration of Independance?", "In what ocean did the Titanic sink?", "Approximately how much of the world population died due to the Black Plague?", "Which ocean is the smallest?")
answer1 = ("1.)Russia", "1.)377","1.)7", "1.)1 in 5 billion", "1.)10 x 10^24 tons", "1.)England and Spain", "1.)Ben Franklin", "1.)North Atlantic", "1.)25%", "1.)Arctic Ocean")
answer2= ("2.)France", "2.)418", "2.)2", "2.)1 in 10 billion", "2.)10 x 10^11 tons", "2.)France and Spain","2.)Thomas Jefferson",  "2.)North Pacific", "2.)40%", "2.)Pacific Ocean")
answer3= ("3.)England", "3.)336", "3.)17", "3.)1 in 1 trillion", "3.)268.77 x 10^38 tons", "3.)France and England", "3.)John Hancock", "3.)West Atlantic", "3.)33%", "3.)Indian Ocean")
answer4= ("4.)Brazil","4.)294", "4.)11", "4.)1 in 5 million", "4.)65.88 x 10^26 tons", "4.)England and Belgium", "4.)George Washington", "4.)South Pacific", "4.)20%", "4.)Atlantic Ocean")
correct = (3, 3, 1, 1, 4, 3, 2, 1, 3, 1)
x = 0

while x < 11:
    q = question.Question(questions[0], answer1[0], answer2[0], answer3[0], answer4[0], correct[0], inpt, isCorrect)
    questionList.append(q)
    inpt = int(input("What is your answer? "))
    if(q.isCorrect(inpt)):
        print("Correct")
    x += 1
    while x == ("1" or "3" or "5" or "7" or "9"):
        print("It is now", player1+"'s", "turn")
    while x == ("2" or "4" or "6" or "8" or "10"):
        print("It is now", player2+"'s", "turn")

print(questionList[q].getQuestion())


