import car

carYear = input("What is the year model of your car? ")
carMake = input("What is the make of your car? ")
speed = 0
brake = speed-5
accelerate = speed + 5

myCar = car.Car(carYear, carMake, speed, brake, accelerate)

print("Your car is a", str(myCar.get_year_model()), myCar.get_make())
print("Your car is going", myCar.get_speed(), "mph.")
print("You accelerate! You are now going", str(myCar.get_accelerate()), "mph.")
print("You continue to accelerate! You are now going",myCar.get_accelerate()+5, "mph.")
print("You continue to accelerate! You are now going",myCar.get_accelerate()+10, "mph.")
print("You continue to accelerate! You are now going", myCar.get_accelerate()+15, "mph.")
print("You continue to accelerate! You are now going", myCar.get_accelerate()+20, "mph.")
print("You brake! You are now going", 25-myCar.get_accelerate(), "mph.")
print("You continue to brake! You are now going", 20-myCar.get_brake(), "mph.")
print("You continue to brake! You are now going", 15-myCar.get_accelerate(), "mph.")
print("You continue to brake! You are now going", 10-myCar.get_accelerate(), "mph.")
print("You continue to brake! You are now going", 5-myCar.get_accelerate(), "mph.")
